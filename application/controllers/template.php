<?php

class Template extends MY_Controller{

    function construct(){
        parent::construct();
     //   $this->load->view('template/navbar',null,true);

    }
    public function index(){
        $html = $this->load->view("component/headerindex",null,true); 
        $this->show($html);
    }
    function dropdown(){

        $html  = $this->load->view("component/headerdrop",null,true);
        $html .= $this->load->view("component/corpodrop",null,true); 
        $this->show($html);
    }
    function modal(){

        $html  = $this->load->view("component/headermodal",null,true);
        $html .= $this->load->view("component/corpomodal",null,true); 
        $this->show($html);
    }
    function sortlist(){

        $html  = $this->load->view("component/headersort",null,true);
        $html .= $this->load->view("component/corposort",null,true); 
        $this->show($html);
    }
}