<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
        <!-- Bootstrap core CSS -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
        <!-- Material Design Bootstrap -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.2/css/mdb.min.css" rel="stylesheet">
        <link rel="stylesheet" media="screen" type="text/css" title="style" href="<?= base_url("assets/css/style2.css") ?>" />
    

        <style>
        .mySlides {display:none;}
        </style>
        <title>LP 2</title>
         <!--Navbar-->
         <nav class="navbar navbar-expand-lg navbar-dark fixed-top scrolling-navbar">
            <div class="container">
              <a class="navbar-brand" href="<?= base_url("template/index") ?>"><strong>LP 2</strong></a>
              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-7" aria-controls="navbarSupportedContent-7" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
              <div class="collapse navbar-collapse" id="navbarSupportedContent-7">
                <ul class="navbar-nav mr-auto">
                  <li class="nav-item active">
                    <a class="nav-link" href="<?= base_url("template/sortlist")?>">SortList<span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item">
                  <li class="nav-item active">
                    <a class="nav-link" href="<?= base_url("template/modal")?>">Modal<span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item active">
                    <a class="nav-link" href="<?= base_url("template/dropdown")?>" >DropDown<span class="sr-only">(current)</span></a>
                  </li>
                  
                </ul>
              </div>
            </div>
          </nav>
          <!-- Navbar -->
    </head>
