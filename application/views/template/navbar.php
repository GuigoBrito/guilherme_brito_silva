<body class="" style=" background-color:#F8F8FF;">
          <!--Navbar-->
          <nav class="navbar navbar-expand-lg navbar-dark fixed-top scrolling-navbar">
            <div class="container">
              <a class="navbar-brand" href="<?= base_url("template/index")?>"><strong>LP 2</strong></a>
              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-7" aria-controls="navbarSupportedContent-7" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
              <div class="collapse navbar-collapse" id="navbarSupportedContent-7">
                <ul class="navbar-nav mr-auto">
                  <li class="nav-item active">
                    <a class="nav-link" href="<?= base_url("template/sortlist")?>">SortList<span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item">
                  <li class="nav-item active">
                    <a class="nav-link" href="<?= base_url("template/modal")?>">Modal<span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item active">
                    <a class="nav-link" href="<?= base_url("template/dropdown")?>">DropDown<span class="sr-only">(current)</span></a>
                  </li>
               
                </ul>
              </div>
            </div>
          </nav>