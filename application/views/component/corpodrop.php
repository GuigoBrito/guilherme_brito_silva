<hr/>
        
        <h1><p class="text-center"> DropDown</p></h1>
                    <h4><p></p> </h4>

            <h4><br/><p class=" text-center">Estilo padrão para o DropDown Bootstrap</p> </h4>
                    <div class="row justify-content-center"> 
                <button class="btn btn-primary dropdown-toggle mr-4" type="button" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">DropDown Básico</button>

                <div class="dropdown-menu">
                <a class="dropdown-item" href="#">Ação</a>
                <a class="dropdown-item" href="#">Outra Ação</a>
                <a class="dropdown-item" href="#">Coloca Alguma coisa aqui</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#">Link Separado</a>
                </div>
            </div>
            <br/><hr/>
            <div class="text-center">

            &lt;button class="btn btn-primary dropdown-toggle mr-4" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Basic dropdown</button><br/>
            &lt;div class="dropdown-menu"&gt;&lt;<br/>
            &lt;a class="dropdown-item" href="#"&gt;Action&lt;/a&gt;<br/>
            &lt;a class="dropdown-item" href="#"&gt;Another action&lt;/a&gt;<br/>
            &lt;a class="dropdown-item" href="#"&gt;Something else here&lt;/a&gt;<br/>
            &lt;div class="dropdown-divider"&gt;&lt;/div&gt;<br/>
            &lt;a class="dropdown-item" href="#"&gt;Separated link&lt;/a&gt;<br/>
            &lt;/div&gt;<br/>

            </div>
            <br/><hr/>

            <h4><br/><p class=" text-center">Estilo de material para o DropDown Bootstrap</p> </h4>
                    <div class="row justify-content-center"> 
                                                         <!--Dropdown primary-->
                            <div class="dropdown">

                            <!--Trigger-->
                            <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">Material dropdown</button>

                            <!--Menu-->
                            <div class="dropdown-menu dropdown-primary">
                            <a class="dropdown-item" href="#">Ação</a>
                            <a class="dropdown-item" href="#">Outra Ação</a>
                            <a class="dropdown-item" href="#">Alguma coisa aqui</a>
                            <a class="dropdown-item" href="#">Outra coisa aqui</a>
                            </div>
                        </div>
                        </div>  
                        <br/><hr/>

                    <div class="text-center">

  
                    &lt;button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Material dropdown</button><br/>

                    &lt;div class="dropdown-menu dropdown-primary"><br/>
                    &lt;a class="dropdown-item" href="#">Action</a><br/>
                    &lt;a class="dropdown-item" href="#">Another action</a><br/>
                    &lt;a class="dropdown-item" href="#">Something else here</a><br/>
                    &lt;a class="dropdown-item" href="#">Something else here</a><br/>
                    &lt;/div><br/>
                    &lt;/div><br/>
                    </div>

                    <br/>
                        </div>
                        <br/><br/><hr/>
                        <h1><p class="text-center">Botão dividido</p></h1><h4><p></p> </h4>
                    <h4><br/><p class=" text-center">Da mesma forma, você pode criar listas suspensas de botão dividido com praticamente a mesma marcação que um botão Dropdown único, 
                    mas com a adição da classe .dropdown-toggle-split para espaçamento adequado ao redor do cursor de lista suspensa.</p> </h4>

                    <!-- Split button -->

                    
                    <div class="row justify-content-center"> 
                        <div class="btn-group">
                        <button type="button" class="btn btn-secondary">Botão</button>
                        <button type="button" class="btn btn-secondary dropdown-toggle px-3" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                            <span class="sr-only">Alternar Dropdown</span>
                        </button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="#">Ação</a>
                            <a class="dropdown-item" href="#">Outra Ação</a>
                            <a class="dropdown-item" href="#">Alguma coisa aqui</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Link Separado</a>
                            </div>
                        </div>
                    </div>

                    <br/><br/><hr/>

                    <div class="text-center"> 
&lt;div class="btn-group"><br/>
    &lt;button type="button" class="btn btn-danger">Danger</button><br/>
    &lt;button type="button" class="btn btn-danger dropdown-toggle px-3" data-toggle="dropdown" aria-haspopup="true"<br/>
    aria-expanded="false"><br/>
    &lt;span class="sr-only">Toggle Dropdown</span><br/>
    &lt;/button><br/>
    &lt;div class="dropdown-menu"><br/>
    &lt;a class="dropdown-item" href="#">Action</a><br/>
    &lt;a class="dropdown-item" href="#">Another action</a><br/>
    &lt;a class="dropdown-item" href="#">Something else here</a><br/>
    &lt;div class="dropdown-divider"></div><br/>
   
</div>



                    <br/><br/><hr/>

                    <h1><p class="text-center">Botão Desabilidado</p></h1><h4><p></p> </h4>
                    <h4><br/><p class=" text-center">Adicione a .disabled na classe</p> </h4>

                    <div class="row justify-content-center"> 
                    <div class="dropdown">
    <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenu5" data-toggle="dropdown"
    aria-haspopup="true" aria-expanded="false">
    Botão DropDown
    </button>
    <div class="dropdown-menu" aria-labelledby="dropdownMenu5">
    <a class="dropdown-item" href="#">Ação normal</a>
    <a class="dropdown-item disabled" href="#">Ação Desabilitada</a>
    <a class="dropdown-item" href="#">Outra ação normal</a>
    </div>
    </div>
    </div>

<br/><br/><hr/>
    <div class="text-center"> 
    &lt;div class="dropdown"><br/>
    &lt;button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenu5" data-toggle="dropdown"
    aria-haspopup="true" aria-expanded="false"><br/>
    Dropdown
    &lt;/button><br/>
    &lt;div class="dropdown-menu" aria-labelledby="dropdownMenu5"><br/>
    &lt;a class="dropdown-item" href="#">Regular link</a><br/>
    &lt;a class="dropdown-item disabled" href="#">Disabled link</a><br/>
    &lt;a class="dropdown-item" href="#">Another link</a><br/>
    &lt;/div><br/>
    &lt;/div><br/>
    </div>

                    <br/><br/><hr/>
                
                    <h1><p class="text-center">Opções de Dropdown</p></h1><h4><p></p> </h4>
                    <h4><br/><p class=" text-center">Use data-offset ou  data-reference para alterar o local do DropDown.</p> </h4>
                    <div class="row justify-content-center"> 
                    <div class="d-flex">
  <div class="dropdown mr-1">
    <button type="button" class="btn btn-primary dropdown-toggle" id="dropdownMenuOffset" data-toggle="dropdown"
      aria-haspopup="true" aria-expanded="false" data-offset="10,20">
      data-offset
    </button>
    <div class="dropdown-menu" aria-labelledby="dropdownMenuOffset">
      <a class="dropdown-item" href="#">Ação</a>
      <a class="dropdown-item" href="#">Outra Ação</a>
      <a class="dropdown-item" href="#">Alguma coisa aqui</a>
    </div>
  </div>
  <div class="btn-group">
    <button type="button" class="btn btn-primary">Referência</button>
    <button type="button" class="btn btn-primary dropdown-toggle dropdown-toggle-split" id="dropdownMenuReference"
      data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-reference="parent">
      <span class="sr-only">DropDown alternado</span>
    </button>
    <div class="dropdown-menu" aria-labelledby="dropdownMenuReference">
      <a class="dropdown-item" href="#">Ação</a>
      <a class="dropdown-item" href="#">Outra Ação</a>
      <a class="dropdown-item" href="#">Alguma coisa aqui</a>
      <div class="dropdown-divider"></div>
      <a class="dropdown-item" href="#">Link Separado</a>
    </div>
  </div>
</div>
</div>
<br/><br/><br/><br/><br/><br/>