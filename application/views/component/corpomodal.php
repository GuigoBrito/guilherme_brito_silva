        <hr/>
        <h1><p class="text-center"> Frame Modal</p></h1>
<h4><p>para funcionar corretamente, além de adicionar uma classe para uma posição, 
você também precisa adicionar a classe especial .modal-iframe à div .modal-dialog.</p> </h4>

<h4><p>Se você deseja alterar a direção da animação modal, adicione a classe .top, .right, bottom ou .left à div .modal.
<br/>.modal-frame + .modal-bottom (Posição Inferior)
<br/>.modal-frame + .modal-top (Posição no Topo)</p> </h4>

<br/><hr/>
<div class="text-center">
        &lt;div class="modal fade bottom" id="frameModalBottom" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"&gt;<br/>

  
        &lt;div class="modal-dialog modal-frame modal-bottom" role="document"&gt;<br/>


        &lt;div class="modal-content"&gt;<br/>
        &lt;div class="modal-body"&gt;<br/>
        &lt;div class="row d-flex justify-content-center align-items-center"&gt;<br/>

        &lt;p class="pt-3 pr-2"&gt;Lorem ipsum dolor sit amet, consectetur adipisicing elit. Impedit nisi quo
        &lt;   provident fugiat reprehenderit nostrum quos..
        &lt;/p&gt;<br/>

        &lt;button type="button" class="btn btn-secondary" data-dismiss="modal"&gt;Close</button&gt;<br/>
        &lt;button type="button" class="btn btn-primary"&gt;Save changes&lt;/button&gt;<br/>
        &lt;/div&gt;<br/>
        &lt;/div&gt;<br/>
        &lt;/div&gt;<br/>
        &lt;/div&gt;<br/>
        &lt;/div&gt;<br/>
</div>


<hr/>
        <h4><p> Abaixo, Temos dois exempos de Modal para ser utilizados de acordo com suas necessidades: Um deles vem de baixo </p> </h4>
        
        <div class="row justify-content-center">
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#frameModalBottom">
  Modal .bottom
        </button>
        </div>
<!-- Frame Modal Bottom -->

<div class="modal fade bottom" id="frameModalBottom" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">

  <!-- Add class .modal-frame and then add class .modal-bottom (or other classes from list above) to set a position to the modal -->
  <div class="modal-dialog modal-frame modal-bottom" role="document">


    <div class="modal-content">
      <div class="modal-body">
        <div class="row d-flex justify-content-center align-items-center">

          <p class="pt-3 pr-2">Esse modelo de modal, vem de baixo.
          </p>

          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="button" class="btn btn-primary">Salvar Mudanças</button>
        </div>
      </div>
    </div>
  </div>
</div>
<br/>
        <h4><p> E o outro que vem de cima </p> </h4>
        
        <div class="row justify-content-center">
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#frameModalTop">
  Modal .top
        </button>
        </div>
<!-- Frame Modal Bottom -->

<div class="modal fade bottom" id="frameModalTop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">

  <!-- Add class .modal-frame and then add class .modal-bottom (or other classes from list above) to set a position to the modal -->
  <div class="modal-dialog modal-frame modal-top" role="document">


    <div class="modal-content">
      <div class="modal-body">
        <div class="row d-flex justify-content-center align-items-center">

          <p class="pt-3 pr-2">Esse modelo de modal, vem de cima.
          </p>

          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="button" class="btn btn-primary">Salvar Mudanças</button>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Frame Modal Bottom -->

<!-- SIDE MODAL -->

<!-- Side Modal Top Right -->
        <hr/>
        <h1><p class="text-center"> Side Modal</p></h1>
        <h4><p>Além de adicionar uma classe para uma posição, 
        você também precisa adicionar uma classe especial .side-modal à div .modal-dialog.</p> </h4>

<h4><p>Se você deseja alterar a direção da animação modal, adicione a classe .top, .right, bottom ou .left à div .modal.
<br/>.modal-side + .modal-top-right(Modal Superior direito)
<br/>.modal-side + .modal-top-left (Modal Superior esquerdo)
<br/>.modal-side + .modal-bottom-right (Modal Inferior direito)
<br/>.modal-side + .modal-bottom-left (Modal Inferior esquerdo)</p></h4>

<br/><hr/>
        <div class="text-center">
    <p>
        &lt;div class="modal fade right" id="sideModalTR" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"&gt;<br/>

  
        &lt;div class="modal-dialog modal-side modal-top-right" role="document"&gt;<br/>
        &lt;div class="modal-content"&gt;<br/>
        &lt;div class="modal-header"&gt;<br/>
        &lt;h4 class="modal-title w-100" id="myModalLabel"&gt;Modal title&lt;/h4&gt;<br/>
        &lt;button type="button" class="close" data-dismiss="modal" aria-label="Close"&gt;<br/>
        &lt;span aria-hidden="true">&times;&lt;/span&gt;<br/>
        &lt;/button&gt;<br/>
        &lt;/div&gt;<br/>
        &lt;div class="modal-body"&gt;<br/>
        ...<br/>
        &lt;/div&gt;<br/>
        &lt;div class="modal-footer"&gt;<br/>
        &lt;button type="button" class="btn btn-secondary" data-dismiss="modal">Close&lt;/button&gt;<br/>
        &lt;button type="button" class="btn btn-primary"&gt;Save changes&lt;/button&gt;<br/>
        &lt;/div&gt;<br/>
        &lt;/div&gt;<br/>
        &lt;/div&gt;<br/>
        &lt;/div&gt;<br/>
        </p>
        </div>
        <br/><hr/>
        <div class="row justify-content-center">
          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#sideModalTR">
          Modal Direita Superior
          </button>
        </div>
<div class="modal fade right" id="sideModalTR" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">

  <!-- Add class .modal-side and then add class .modal-top-right (or other classes from list above) to set a position to the modal -->
  <div class="modal-dialog modal-side modal-top-right" role="document">


    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title w-100" id="myModalLabel">Titulo do Modal</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Modal Superior Direito
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        <button type="button" class="btn btn-primary">Salvar Mudanças</button>
      </div>
    </div>
  </div>
</div>


<div class="row justify-content-center">
          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#sideModalLt">
          Modal Direita Inferior
          </button>
        </div>
<div class="modal fade right" id="sideModalLt" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">

  <!-- Add class .modal-side and then add class .modal-top-right (or other classes from list above) to set a position to the modal -->
  <div class="modal-dialog modal-side modal-top-Left" role="document">


    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title w-100" id="myModalLabel">Modal Inferior Direito</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Modal Diteiro Inferior
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        <button type="button" class="btn btn-primary">Salvar Mudanças</button>
      </div>
    </div>
  </div>
</div>
<!-- Side Modal Left -->

<div class="row justify-content-center">
          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#sideModalTLt">
          Modal Esquerda Superior
          </button>
        </div>
<div class="modal fade right" id="sideModalTLt" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">

  <!-- Add class .modal-side and then add class .modal-top-right (or other classes from list above) to set a position to the modal -->
  <div class="modal-dialog modal-side modal-top-left" role="document">


    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title w-100" id="myModalLabel">Modal Superior Esquerdo</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       Modal Superior Esquerdo
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        <button type="button" class="btn btn-primary">Salvar Mudanças</button>
      </div>
    </div>
  </div>
</div>


<div class="row justify-content-center">
          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#sideModalBLt">
          Modal Esquerdo Inferior
          </button>
        </div>
<div class="modal fade right" id="sideModalBLt" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">

  <!-- Add class .modal-side and then add class .modal-top-right (or other classes from list above) to set a position to the modal -->
  <div class="modal-dialog modal-side modal-bottom-left" role="document">


    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title w-100" id="myModalLabel">Modal Inferior Esquerdo</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Modal Inferior Esquerdo
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        <button type="button" class="btn btn-primary">Salvar Mudanças</button>
      </div>
    </div>
  </div>
</div>
