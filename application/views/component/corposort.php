
        <div class="ml-5">
        <hr/>
        <h1><p class="text-center"> SortList</p></h1>
        <h4><p><br/>-Faça um loop que continuará até que nenhuma troca seja feita;
               <br/>-No primeiro Loop ele fala que nenhuma trocá será feita;
               <br/>-Percorre todos os itens da lista;
               <br/>-Comece dizendo que não deve haver mudança;
               <br/>-Verifique se o próximo item deve mudar de lugar com o item atual;
               <br/>-Se o próximo item for alfabeticamente menor que o item atual;
               <br/>-marcar como um interruptor e quebrar o loop;</p></h4>
        </div>
        <div class="row justify-content-center" >
        <button type="button" class="btn btn-secondary" onclick="sortList()">Ordenar em ordem crescente</button>

        <ul id="id01" class="h4-responsive">
            <li>Maria Clara</li>
            <li>Vinícius</li>
            <li>Beatriz</li>
            <li>Ana</li>
            <li>Zuleide</li>
            <li>Guilherme</li>
        </ul>
        </div>
<script>
function sortList() {
  var list, i, switching, b, shouldSwitch;
  list = document.getElementById("id01");
  switching = true;
  while (switching) {
    switching = false;
    b = list.getElementsByTagName("LI");
    for (i = 0; i < (b.length - 1); i++) {
      shouldSwitch = false;
      if (b[i].innerHTML.toLowerCase() > b[i + 1].innerHTML.toLowerCase()) {
        shouldSwitch = true;
        break;
      }
    }
    if (shouldSwitch) {
      b[i].parentNode.insertBefore(b[i + 1], b[i]);
      switching = true;
    }
  }
}
</script>

        <div class="text-center">
        <br/><hr/>
        &lt;script&gt; <br/>
        function sortList() { <br/>
        var list, i, switching, b, shouldSwitch; <br/>
        list = document.getElementById("id01"); <br/>
        switching = true; <br/>
        while (switching) { <br/>
        switching = false; <br/>
        b = list.getElementsByTagName("LI"); <br/>
         for (i = 0; i < (b.length - 1); i++) { <br/>
        shouldSwitch = false; <br/>
        if (b[i].innerHTML.toLowerCase() > b[i + 1].innerHTML.toLowerCase()) { <br/>
        shouldSwitch = true; <br/>
        break; <br/>
             } <br/>
             } <br/>
        if (shouldSwitch) { <br/>
        b[i].parentNode.insertBefore(b[i + 1], b[i]); <br/>
         switching = true; <br/>
    } <br/>
            } <br/>
            } <br/>
    &lt;/script&gt;
    </div>
    <br/><hr/>
        
<div class="ml-5">
      
        <h1><p class="text-center"> SortList Crescente e Decrescente</p></h1>
        <h4><p><br/>-Defina a direção da classificação para crescente;
               <br/>-Faça um loop que continuará até que nenhuma alternância seja feita;
               <br/>-Comece dizendo que não deve haver troca;
               <br/>-Percorrer todos os itens da lista;
               <br/>-Comece dizendo que não deve haver mudança;
               <br/>-Verifique se o próximo item deve mudar de lugar com o item atual,
com base na direção da classificação (asc ou desc);
               <br/>-Se o próximo item for alfabeticamente menor que o item atual,
 marque com um Switch e interrompa o ciclo;
               <br/>-Se o próximo item for alfabeticamente maior que o item atual,
marque como um Switch e interrompa o ciclo;
               <br/>-Se um Switch foi marcado, faça a troca
e marque que uma troca foi feita;
               <br/>-Cada vez que uma troca é feita, aumente a contagem em 1;
               <br/>-Se nenhuma mudança foi feita E a direção for "asc";
               <br/>-defina a direção para "desc" e execute o loop while novamente</p></h4>
        </div>
<div class="row justify-content-center" >
        <button type="button" class="btn btn-secondary" onclick="sortListDir()">Crescente e ou Decrescente</button>
        <ul id="id02"  class=" h4-responsive">
                    <li>Maria Clara</li>
                    <li>Vinícius</li>
                    <li>Beatriz</li>
                    <li>Ana</li>
                    <li>Zuleide</li>
                    <li>Guilherme</li>
        </ul>
</div>
<script>
function sortListDir() {
  var list, i, switching, b, shouldSwitch, dir, switchcount = 0;
  list = document.getElementById("id02");
  switching = true;
  dir = "asc";
  while (switching) {
    switching = false;
    b = list.getElementsByTagName("LI");
    for (i = 0; i < (b.length - 1); i++) {
      shouldSwitch = false;
      if (dir == "asc") {
        if (b[i].innerHTML.toLowerCase() > b[i + 1].innerHTML.toLowerCase()) {
          shouldSwitch = true;
          break;
        }
      } else if (dir == "desc") {
        if (b[i].innerHTML.toLowerCase() < b[i + 1].innerHTML.toLowerCase()) {
          shouldSwitch= true;
          break;
        }
      }
    }
    if (shouldSwitch) {
      b[i].parentNode.insertBefore(b[i + 1], b[i]);
      switching = true;
      switchcount ++;
    } else {
      if (switchcount == 0 && dir == "asc") {
        dir = "desc";
        switching = true;
      }
    }
  }
}
</script>
<br/><hr/>
<div class="text-center">
&lt;script&gt;
function sortListDir() {<br/>
  var list, i, switching, b, shouldSwitch, dir, switchcount = 0;<br/>
  list = document.getElementById("id02");<br/>
  switching = true;<br/>
  dir = "asc";<br/>
  while (switching) {<br/>
    switching = false;<br/>
    b = list.getElementsByTagName("LI");<br/>
    for (i = 0; i < (b.length - 1); i++) {<br/>
      shouldSwitch = false;<br/>
      if (dir == "asc") {<br/>
        if (b[i].innerHTML.toLowerCase() > b[i + 1].innerHTML.toLowerCase()) {<br/>
          shouldSwitch = true;<br/>
          break;<br/>
        }<br/>
      } else if (dir == "desc") {<br/>
        if (b[i].innerHTML.toLowerCase() < b[i + 1].innerHTML.toLowerCase()) {<br/>
          shouldSwitch= true;<br/>
          break;<br/>
        }<br/>
      }<br/>
    }<br/>
    if (shouldSwitch) {<br/>
      b[i].parentNode.insertBefore(b[i + 1], b[i]);<br/>
      switching = true;<br/>
      switchcount ++;<br/>
    } else {<br/>
      if (switchcount == 0 && dir == "asc") {<br/>
        dir = "desc";<br/>
        switching = true;<br/>
      }<br/>
    }<br/>
  }<br/>
}<br/>
&lt;/script&gt;
</div>
